const express = require ("express");
const app = express();
const PORT = 3001;
const mongoose = require("mongoose");

app.use (express.json());
app.use (express.urlencoded({extended:true}));

//connect server to mongoDB atlas
mongoose.connect('mongodb+srv://jlumactud:jellyanne@batch139.poyy4.mongodb.net/todo_tasks?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

//get notification if successfully connected or not
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log (`Connected to database`)
);

//Schema
const User = new mongoose.Schema (
	{
		user: String,
		status: 
		{
			type: String,
			default: "pending"
		}
	}
);

//Models
const UserModel = mongoose.model("User", User);


//ROUTES

app.post ("/signup", (req,res) =>
{
	UserModel.create ({}, (result) => {
		let newUser = new UserModel ({
			user: req.body.user
		})

		newUser.save((err, savedUser) => {
				if (err) {
					return console.error (err)
				}else {
					return res.send (`New user created`)
				}
		})
	})
})

app.listen (PORT, () => console.log (`Server running at port ${PORT}`));